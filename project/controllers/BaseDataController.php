<?php

class BaseDataController {
    /** @var DatabaseController|PDO $myDB*/
    private $myDB;

    function __construct(){
        $this->db_connect();
    }

    /**
     * inst singleton
     * @return BaseDataController
     */
    public static function Inst(){
        static $object;
        $className = get_called_class();

        if(!is_a($object, $className)){
            $object = new $className();
        }

        return $object;
    }

    protected function beginTransaction() {
        if(!$this->myDB->beginTransaction()) {
            throw new Error('Transactions not supported');
        }
    }

    protected function commit(){
        if(!$this->myDB->inTransaction()){
            throw new Error( 'Commit called not in transaction!');
        }

        $this->myDB->commit();
    }

    protected function getLastInsertedId() {
        return $this->myDB->lastInsertId();
    }

    protected function make($sql, $mode, $data = array()) {
        if (empty($sql)) {
            throw new Exception( "get[$mode]: no sql query given");
        }

        $this->db_connect();
        return $this->myDB->make($sql, $mode, $data);
    }

    protected function rollback() {
        if(!$this->myDB->inTransaction()){
            throw new Error( 'Rollback called not in transaction!');
        }

        $this->myDB->rollback();
    }

    private function db_connect(){
        if (!($this->myDB instanceof DatabaseController)){
            $this->myDB = DatabaseController::getDbPDO();
        }
    }
}
