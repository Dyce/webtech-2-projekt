<?php

require_once('../config.php');

class DatabaseController {
    const DBInitStatements = array(
        'SET autocommit = 0',
        'SET NAMES "utf8"'
    );

    /** @var PDO */
    private $dbPDO;

    function __construct(){
        $options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
        foreach (self::DBInitStatements as $statement) {
            $options[PDO::MYSQL_ATTR_INIT_COMMAND] = $statement;
        }

        $dbPDO = new PDO(DB_DNS, DB_USER, DB_PASSWORD, $options);
        $dbPDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->dbPDO = $dbPDO;
    }

    public function __call($name, $arguments) {
        return call_user_func_array(array($this->dbPDO, $name), $arguments);
    }

    public static function getDbPDO() {
        static $myDB;
        if(!is_a($myDB, 'DatabaseController')) {
            $myDB = new DatabaseController();
        }

        return $myDB;
    }

    public function make($sql, $mode, $data) {
        if (empty($sql)) {
            throw new Exception( "get[$mode]: no sql query given");
        }

        $pdoStatement = $this->dbPDO->prepare($sql);
        $executeResult = $pdoStatement->execute($data);

        switch ($mode) {
            case DB_ALL:
                return $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
            case DB_COL:
                return $pdoStatement->fetchAll(PDO::FETCH_COLUMN,0);
            case DB_ROW:
                return $pdoStatement->fetch(PDO::FETCH_ASSOC);
            case DB_ONE:
                $result = $pdoStatement->fetch(PDO::FETCH_NUM);
                return (is_array($result) && !empty($result)) ? $result[0] : null;
            case DB_QUERY:
                return $executeResult;
            default:
                throw new Exception("Undefined mode: $mode for myDB get");
        }
    }
}
