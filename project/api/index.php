<?php

require('../config.php');

if(!array_key_exists('action', $_REQUEST)) {
    sendErrorResult(HTTP_NOT_FOUND,'action');
}

if(($_REQUEST['auth_key'] ?? null) !== API_AUTH_KEY) {
    sendErrorResult(HTTP_UNAUTHORIZED,'auth_key');
}

switch ($_REQUEST['action']) {
    case 'getEmailData': {
        if(!array_key_exists('email', $_REQUEST)) {
            sendErrorResult(HTTP_BAD_REQUEST,'getEmailData');
        }
        if(!array_key_exists('statistics', $_REQUEST)) {
            sendErrorResult(HTTP_BAD_REQUEST,'getEmailData');
        }

        sendResult(Mail::sendEmail($_REQUEST['email'], $_REQUEST['statistics']),'getEmailData');
    }
    case 'getApiExport': {
        if(!array_key_exists('api', $_REQUEST)) {
            sendErrorResult(HTTP_BAD_REQUEST,'getApiExport');
        }

        sendResult(App::Inst()->apiExportPdf($_REQUEST['api']),'getApiExport');
    }
    case 'getLogExport': {
        if(!array_key_exists('type', $_REQUEST) || !is_string($_REQUEST['type']) || ($_REQUEST['type'] != "pdf" && $_REQUEST['type'] != "csv")) {
            sendErrorResult(HTTP_BAD_REQUEST,'getLogExport');
        }

        if($_REQUEST['type'] == 'pdf') {
            $result = App::Inst()->logExportPdf();
        } else {
            $result = App::Inst()->logCsvExport();
        }

        sendResult($result,'getLogExport');
    }
    case 'getBallOnStick': {
        if(!array_key_exists('position', $_REQUEST) || !is_numeric($_REQUEST['position'])) {
            sendErrorResult(HTTP_BAD_REQUEST,'ball');
        }
        $result = CAS::Inst()->getBallOnStick($_REQUEST['position']);

        sendResult($result,'ball');
    }
    case 'getCasCustom': {
        if(!array_key_exists('customInput', $_REQUEST)) {
            sendErrorResult(HTTP_BAD_REQUEST,'custom');
        }

        if(array_key_exists('customInput', $_REQUEST) && empty($_REQUEST['customInput'])) {
            sendErrorResult(HTTP_BAD_REQUEST,'custom');
        }

        $result = CAS::Inst()->getCasCustom($_REQUEST['customInput']);
        sendResult($result,'custom');
    }
    case 'getCarShockAbsorber': {
        if(
            !array_key_exists('height', $_REQUEST) || !is_numeric($_REQUEST['height']) ||
            !array_key_exists('previousPosition', $_REQUEST) || !is_numeric($_REQUEST['previousPosition'])
        ) {
            sendErrorResult(HTTP_BAD_REQUEST,'car');
        }

        $result = CAS::Inst()->getCarShockAbsorber($_REQUEST['height'], $_REQUEST['previousPosition']);
        sendResult($result,'car');
    }
    case 'getInvertedPendulum': {
        if(
            !array_key_exists('position', $_REQUEST) || !is_numeric($_REQUEST['position']) ||
            !array_key_exists('previousPosition', $_REQUEST) || !is_numeric($_REQUEST['previousPosition'])
        ) {
            sendErrorResult(HTTP_BAD_REQUEST,'pendulum');
        }

        $result = CAS::Inst()->getInvertedPendulum($_REQUEST['position'], $_REQUEST['previousPosition']);
        sendResult($result,'pendulum');
    }
    case 'getTiltOfAircraft': {
        if(!array_key_exists('angle', $_REQUEST) || !is_numeric($_REQUEST['angle'])) {
            sendErrorResult(HTTP_BAD_REQUEST,'aircraft');
        }

        $startPos = "aaaaaaaaaaaaaaA";
        if(array_key_exists('startPosition', $_REQUEST) && trim(!empty($_REQUEST['startPosition']))) {
            $startPos = $_REQUEST['startPosition'];
        } else {
            $startPos = "[0,0,0]";
        }

        $result = CAS::Inst()->getTiltOfAircraft($_REQUEST['angle'], $startPos);
        sendResult($result,'aircraft');
    }
}

sendErrorResult(HTTP_NOT_FOUND,'none');

function sendResult($result, $action) {
    if(array_key_exists('api', $_REQUEST)) {
        unset($_REQUEST['api']);
    }
    if(array_key_exists('email', $_REQUEST)) {
        unset($_REQUEST['email']);
    }

    App::Inst()->insertLog(json_encode($_REQUEST), $action);

    $output = array('result' => $result);

    echo json_encode($output, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
    exit(0);
}

function sendErrorResult($errorCode,$action = null){
    App::Inst()->insertLog(json_encode($_REQUEST), $action, $errorCode);
    http_response_code($errorCode);
}
