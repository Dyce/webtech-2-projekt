create schema project;
create table logger
(
	id int auto_increment,
	log varchar(256) default null null,
	time timestamp not null,
	errorLog varchar(256) default null null,
	constraint logger_pk
		primary key (id)
);
alter table logger
	add action varchar(16) default null null;

