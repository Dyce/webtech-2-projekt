<?php

class Page {

    public static function getBaseHeaderImportsAndSetup() {
        return "
            <meta charset='UTF-8'>
            <link rel='stylesheet' type='text/css' href='../node_modules/bootstrap/dist/css/bootstrap.min.css' media='all'>
            <link rel='stylesheet' type='text/css' href='../node_modules/@fortawesome/fontawesome-free/css/all.min.css'/>
            <link rel='stylesheet' type='text/css' href='../pages/styles.css'/>
            <link rel='icon' type='image/png' href='../assets/icons/favicon.png'> 
            <script src='../node_modules/jquery/dist/jquery.min.js'></script>
            <script src='../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js'></script>
            <script src='/project/pages/app.js'></script>
        ";
    }

    public static function getNavigationHeader() {
        $CASPages = array(
            array(
                'href' => 'cas-ball-on-stick.php', 'icon' => 'fa-golf-ball',
                'name' => Translator::Inst()->getTranslate('common.ballOnStick')
            ),
            array(
                'href' => 'cas-car-shock-absorber.php', 'icon' => 'fa-car-crash',
                'name' => Translator::Inst()->getTranslate('common.carShockAbsorber')
            ),
            array(
                'href' => 'cas-inverted-pendulum.php', 'icon' => 'fa-balance-scale-left',
                'name' => Translator::Inst()->getTranslate('common.invertedPendulum')
            ),
            array(
                'href' => 'cas-tilt-of-aircraft.php', 'icon' => 'fa-plane-departure',
                'name' => Translator::Inst()->getTranslate('common.tiltOfAircraft')
            ),
            array(
                'href' => 'cas-custom.php', 'icon' => 'fa-question',
                'name' => Translator::Inst()->getTranslate('common.custom')
            ),
        );

        $CASMenuItems = "";
        foreach ($CASPages as $CASPage) {
            $CASMenuItems .= Page::getMenuListItem($CASPage['href'], $CASPage['icon'], $CASPage['name'], false);
        }

        $pages = array(
            array(
                'href' => 'about.php', 'icon' => 'fa-info-circle',
                'name' => Translator::Inst()->getTranslate('common.aboutSite')
            ),
            array(
                'href' => 'api.php', 'icon' => 'fa-scroll', 'name' => 'Api'
            ),
            array(
                'name' => 'CAS', 'type' => 'CasSubMenu'
            ),
            array(
                'href' => 'statistics.php', 'icon' => 'fa-chart-line',
                'name' => Translator::Inst()->getTranslate('common.statistics')
            ),
        );

        $menuItems = "";
        foreach ($pages as $page) {
            if(array_key_exists('type', $page) && $page['type'] == 'CasSubMenu') {
                $isActive = !empty(preg_grep("/cas(-|[a-z]|[A-Z])*\.php/",[$_SERVER['REQUEST_URI']]));

                $menuItems .= "                
                    <li class='page-header-menu-item'>
                        <a href='#cas' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false' ".($isActive ? "class='active'" : "").">
                            <i class='fa fa-calculator pr-md-2'></i>
                            <span class='d-none d-md-block'>
                                {$page['name']}
                            </span>
                        </a>
                        <ul class='dropdown-menu page-header-dropdown-submenu'>
                            $CASMenuItems
                        </ul>
                    </li>
                ";
            } else {
                $menuItems .= Page::getMenuListItem($page['href'], $page['icon'], $page['name']);
            }
        }

        $languageCode = Translator::Inst()->getActiveLanguageCode();

        return "
            <header class='page-header'>
                <ul class='page-header-menu'>
                    <li class='page-header-menu-item d-none d-md-flex'>
                        <span style='font-size: 14px;'>
                            WebTech2 - ".Translator::Inst()->getTranslate('common.project')."
                        </span>
                    </li>
                    $menuItems
                    <div class='page-header-language-select d-flex align-items-center'>
                        <div class='image-container d-flex mx-2 ".($languageCode == 'en' ? "active" : "")."' onclick='selectLanguage(\"en\")'>
                            <img src='../assets/icons/en.gif' alt='en'>
                        </div>
                        <div class='image-container d-flex mr-2 ".($languageCode == 'sk' ? "active" : "")."' onclick='selectLanguage(\"sk\")'>
                            <img src='../assets/icons/sk.gif' alt='sk'>
                        </div>
                    </div>
                </ul>
            </header>
        ";
    }

    private static function getMenuListItem($href, $icon, $name, $hideTextForSmall = true) {
        $isActive = strpos($_SERVER['REQUEST_URI'], $href) !== false;
        $iconTag = "";
        if(!empty($icon)) {
            $iconTag = "<i class='fa $icon pr-md-2 ".($hideTextForSmall ? "" : "pr-2")."'></i>";
        }
        $textClassSet = $hideTextForSmall ? "d-none d-md-block" : "";
        
        return "
            <li class='page-header-menu-item'>
                <a href='$href' ".($isActive ? "class='active'" : "").">
                    $iconTag
                    <span class='$textClassSet'>
                        $name
                    </span>
                </a>
            </li>
        ";
    }
}
