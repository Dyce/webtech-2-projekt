<?php

class Translator {

    private $languageCode;
    private $translates;

    function __construct(){
        $languageCode = 'en';
        if(($_COOKIE['language_code'] ?? 'en') == 'sk') {
            $languageCode = 'sk';
        }

        $this->languageCode = $languageCode;
        $this->translates = json_decode(file_get_contents("../assets/i18n/$this->languageCode.json"), true);
    }

    /**
     * inst singleton
     * @return Translator
     */
    public static function Inst(){
        static $object;
        $className = get_called_class();

        if(!is_a($object, $className)){
            $object = new $className();
        }

        return $object;
    }

    public function getTranslate($translateKey, $capitalize = true) {
        $translate =  $this->translates[$translateKey] ?? "";
        if(empty($translate)) {
            return "Missing $translateKey in $this->languageCode";
        }

        if($capitalize) {
            $translate = mb_strtoupper(mb_substr($translate, 0, 1)).mb_substr($translate, 1);
        }

        return $translate;
    }

    public function getActiveLanguageCode() {
        return $this->languageCode;
    }
}
