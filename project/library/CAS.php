<?php

class CAS {

    /**
     * inst singleton
     * @return CAS
     */
    public static function Inst(){
        static $object;
        $className = get_called_class();

        if(!is_a($object, $className)){
            $object = new $className();
        }

        return $object;
    }

    public function getBallOnStick($r){
        $octaveResult = $this->octaveEval("[x, y, time,N] = ball($r)");
        $parsedResult = $this->parseOctaveResult($octaveResult);

        return array(
            'angle' => $parsedResult['x'][2] ?? array(),
            'deceleration_coefficient_ms' => CAS_DECELERATION_COEFFICIENT_MS,
            'time' => $parsedResult['time'][0] ?? array(),
            'x' => $parsedResult['y'][0] ?? array(),
            'y' => $parsedResult['x'][0] ?? array()
            );
    }

    public function getInvertedPendulum($position, $previousPosition) {
        $octaveResult = $this->octaveEval("[x, y, time] = pendulum($position, $previousPosition)");
        $parsedResult = $this->parseOctaveResult($octaveResult);

        return array(
            'angle' => $parsedResult['x'][2] ?? array(),
            'deceleration_coefficient_ms' => CAS_DECELERATION_COEFFICIENT_MS,
            'time' => $parsedResult['time'][0] ?? array(),
            'x' => $parsedResult['x'][0] ?? array(),
        );
    }

    public function getTiltOfAircraft($angle, $startPosition) {
        $startPosition = json_decode($startPosition);
        $octaveResult = $this->octaveEval("[tilt, flap, time, angles] = airplane(".deg2rad($angle).
            ", [".$startPosition[0].";".$startPosition[1].";".$startPosition[2]."])");
        $parsedResult = $this->parseOctaveResult($octaveResult);

        return array(
            'deceleration_coefficient_ms' => CAS_DECELERATION_COEFFICIENT_MS,
            'flap' => $parsedResult['flap'][0] ?? array(),
            'tilt' => $parsedResult['tilt'][0] ?? array(),
            'time' => $parsedResult['time'][0] ?? array(),
            'angles' => $parsedResult['angles'][0] ?? array(),
            'startAt' => "[tilt, flap, time, angles] = airplane(".deg2rad($angle).
                ", [".$startPosition[0].";".$startPosition[1].";".$startPosition[2]."])"
        );
    }

    public function getCarShockAbsorber($height, $position) {
        $octaveResult = $this->octaveEval("[poziciaAuta, poziciaKolesa, y, t] = shockabsorber($height, $position)");
        $parsedResult = $this->parseOctaveResult($octaveResult);

        return array(
            'deceleration_coefficient_ms' => CAS_DECELERATION_COEFFICIENT_MS,
            'poziciaAuta' => $parsedResult['poziciaAuta'][0] ?? array(),
            'poziciaKolesa' => $parsedResult['poziciaKolesa'][0] ?? array(),
            'time' => $parsedResult['t'][0] ?? array(),
        );
    }

    /**
     * Send expression to be evaluated in Octave
     * @param string $expression Expression to be evaluated
     * @return array Unparsed result of evaluating expression in Octave
     */
    public function getCasCustom($expression)
    {
        $octaveResult = $this->octaveEval($expression);
        return $octaveResult;
    }

    /**
     * Evaluate expression in Octave
     * @param string $expression Expression to be evaluated
     * @return null|mixed Null on error, octave result on success
     */
    private function octaveEval($expression) {
        $cmd = "octave -q --no-window-system --eval '$expression'";
        $octaveOutput = null;
        exec($cmd, $octaveOutput);

        return $octaveOutput;
    }

    /**
     * Parse octave output from CMD into associated array
     * @param array $octaveResultRecords
     * @return array
     */
    private function parseOctaveResult($octaveResultRecords) {
        if(empty($octaveResultRecords)) {
            return array();
        }

        $actualVar = "";
        $result = array();

        foreach ($octaveResultRecords as $record) {
            preg_match("/^(.)*=$/", $record, $match);
            if(!empty($match)) {
                $actualVar = trim(preg_replace("/\s+|=/", "", $match[0]));
                $result[$actualVar] = array();
            } else if(!empty($record)){
                $record = trim(preg_replace("/\s+/", " ", $record));
                $parts = explode(' ', $record);

                foreach ($parts as $key => $part) {
                    if(!isset($result[$actualVar][$key])) {
                        $result[$actualVar][$key] = array();
                    }

                    $result[$actualVar][$key][] = (double)$part;
                }
            }
        }

        return $result;
    }
}
