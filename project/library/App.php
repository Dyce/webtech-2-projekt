<?php

class App extends BaseDataController {

    private $tblLogger = "logger";

    /**
     * inst singleton
     * @return App
     */
    public static function Inst(){
        static $object;
        $className = get_called_class();

        if(!is_a($object, $className)){
            $object = new $className();
        }

        return $object;
    }

    public function apiExportPdf($apiBodyHtml){
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('API Documentation');
        // set default header data

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // add a page
        $pdf->AddPage();

        $pdf->writeHTML($apiBodyHtml);
        $pdf->lastPage();

        $pdfName = "export-api-".time().".pdf";
        $pdf->Output(PATH_PUBLIC."/".$pdfName,"F");

        return PUBLIC_DIR.$pdfName;
    }

    public function logExportPdf(){
        $sql = "SELECT * FROM $this->tblLogger";
        $result = $this->make($sql, DB_ALL);

        $html = "<h2>Logs Export</h2>
                    <table>
                        <tr>
                            <th align=\"center\"width=\"50\">ID:</th>
                            <th align=\"center\"width=\"200\">LOG:</th>
                            <th align=\"center\"width=\"100\">ACTION:</th>
                            <th align=\"center\"width=\"100\">TIME:</th>
                            <th align=\"center\"width=\"50\">ERR CODE:</th>
                        </tr>";

        foreach ($result as $item) {
            $html .= "<tr>";
            $html .= "<td align=\"center\">{$item['id']}</td>";
            $html .= "<td width=\"200\">{$item['log']}</td>";
            $html .= "<td align=\"center\">{$item['action']}</td>";
            $html .= "<td align=\"center\">{$item['time']}</td>";
            $html .= "<td align=\"center\">{$item['errorLog']}</td>";
            $html .= "</tr>";
        }

        $html .= "</table>";

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Logs Export');
        // set default header data

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->AddPage();
        $pdf->writeHTML($html);
        $pdf->lastPage();

        $pdfName = "export-".time().".pdf";
        $pdf->Output(PATH_PUBLIC."/".$pdfName,"F");

        return PUBLIC_DIR.$pdfName;
    }

    public function logCsvExport(){
        $sql = "SELECT * FROM $this->tblLogger";
        $result = $this->make($sql, DB_ALL);

        $csvName = "export-log-".time().".csv";
        $output = fopen(PATH_PUBLIC."/".$csvName, 'w');

        fputcsv($output, array('ID', 'LOG', 'TIME', 'ERRCODE', 'ACTION'), ';');
        foreach ($result as $record){
            fputcsv($output, $record, ";");
        }

        return PUBLIC_DIR.$csvName;
    }

    public function insertLog($log, $action, $errorLog = null) {
        $sql = "INSERT INTO `logger`(`id`, `log`, `time`, `errorLog`,`action`) VALUES (null,'{$log}',sysdate(),'{$errorLog}','{$action}')";
        $this->make($sql,DB_QUERY);
    }

    public function getLogs(){
        $sql = "SELECT * FROM `logger` ORDER BY time DESC LIMIT 50";
        return $this->make($sql, DB_ALL);
    }

    public function getStatistics(){
        $sql = "SELECT action, COUNT(*) as count FROM `logger` GROUP BY action ORDER BY count DESC";
        return $this->make($sql, DB_ALL);
    }
}
