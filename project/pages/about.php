<?php

require('../config.php');

echo "
    <!DOCTYPE html>
    <html lang='sk'>
        <head>
            ".Page::getBaseHeaderImportsAndSetup()."
            <title>".Translator::Inst()->getTranslate('common.aboutSite')."</title>
        </head>
        <body>
            ".Page::getNavigationHeader()."
            <div class='container'>
                <h4 class='text-center'>
                   ".Translator::Inst()->getTranslate('common.aboutSite')."
                </h4>
                <div class='block description-block mb-2'>
                    " .Translator::Inst()->getTranslate('description.about'). "
                </div>
                <h4 class='text-center mb-1'>
                    ".Translator::Inst()->getTranslate('common.tasksTable')."
                </h4>
                <div class='block mt-3'>
                     <table class='table table-striped table-bordered'>
                        <tr>
                            <th>Úloha</th>
                            <th>Czapala</th>
                            <th>Švidraň</th>
                            <th>Švábik</th>
                            <th>Samko</th>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.apiDesc')." + PDF export</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class='checked'></td>
                        </tr>
                        <tr>
                            <td>CAS ".Translator::Inst()->getTranslate('common.ballOnStick')."</td>
                            <td></td>
                            <td></td>
                            <td class='checked'></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>CAS ".Translator::Inst()->getTranslate('common.carShockAbsorber')."</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class='checked'></td>
                        </tr>
                        <tr>
                            <td>CAS ".Translator::Inst()->getTranslate('common.tiltOfAircraft')."</td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>CAS ".Translator::Inst()->getTranslate('common.invertedPendulum')."</td>
                            <td></td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>CAS ".Translator::Inst()->getTranslate('common.custom')."</td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.databaseLogging')."</td>
                            <td></td>
                            <td></td>
                            <td class='checked'></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.emailSender')."</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class='checked'></td>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.logExports')."</td>
                            <td></td>
                            <td></td>
                            <td class='checked'></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.navigation')."</td>
                            <td></td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                        </tr>                        
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.octaveSetup')."</td>
                            <td class='checked'></td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.projectDesign')."</td>
                            <td></td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.statistics')."</td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.tasksTable')."</td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.translationSystem')."</td>
                            <td></td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>".Translator::Inst()->getTranslate('common.webDesign')."</td>
                            <td></td>
                            <td class='checked'></td>
                            <td></td>
                            <td></td>
                        </tr>                  
                    </table>
                </div>
            </div>
        </body>
    </html>
";
