let animation = null;
let animationCtx = null;
let animationObjectsBall = {
    ground: null,
    ball: null,
    group: null
};
let maxIntervalIndex = null;
let intervalUpdater = null;
let intervalIndex = null;
let chartJS = null;
let data = {angle: [], deceleration_coefficient_ms: 0, time: [], x: []};
let submitButton = null;
let visible = {animation: true, graph: true};
let translates = {};

function init(translatesObj) {
    submitButton = $(document.getElementById('subBtn'));
    translates = translatesObj;
}

function show(targetForm) {
    let targetFormData = getDataFromForm(targetForm);

    if(visible.graph) {
        $('#cas-result-chart').css('display', 'block');
        if(chartJS !== null) {
            chartJS.destroy();
        }
        initBallChart();
    } else {
        $('#cas-result-chart').css('display', 'none');
    }

    $('#cas-result-animation').css('display', 'block');
    if(animation === null) {
        initAnimationBall();
    }

    disableSubmit();
    getData('getBallOnStick', {...targetFormData}, (response) => {
        data = response.result;
        maxIntervalIndex = data.time.length - 1;
        intervalIndex = 0;

        if(visible.graph) {
            $('#cas-result-chart').css('display', 'block');
            if(chartJS !== null) {
                chartJS.destroy();
            }
            initBallChart();
        } else {
            $('#cas-result-chart').css('display', 'none');
        }

        if(visible.animation) {
            $('#cas-result-animation').css('display', 'block');
            if(animation === null) {
                initAnimationBall();
            }

        } else {
            $('#cas-result-animation').css('display', 'none');
        }

        if(visible.graph || visible.animation) {
            startDataUpdaterBall();
        } else {
            enableSubmit();
        }

    }, () => enableSubmit());
}
function startDataUpdaterBall() {
    intervalUpdater = setInterval(() => {
        if(intervalIndex >= maxIntervalIndex) {
            stopDataUpdaterBall();
            enableSubmit();
            return;
        }

        if(visible.graph) {
            chartJS.data.datasets[0].data.push({x: data.time[intervalIndex], y: data.x[intervalIndex]});
            chartJS.data.datasets[1].data.push({x: data.time[intervalIndex], y: (data.angle[intervalIndex]*180)/3.14});
            chartJS.update();
        }

        if(visible.animation) {
            anime((data.x[intervalIndex] - (intervalIndex === 0 ? 0 : data.x[intervalIndex - 1]))*4,
                data.y[intervalIndex]- (intervalIndex === 0 ? 0 : data.y[intervalIndex - 1]),
                (data.angle[intervalIndex]*180)/3.14);
        }

        intervalIndex++;

    }, data.deceleration_coefficient_ms*0.75);

}
function anime(xOffset,yOffset, angle) {
    Object.keys(animationObjectsBall).forEach((objectName) => {
        if(objectName === 'ball') {
            animationObjectsBall[objectName].animate('left', animationObjectsBall[objectName].left + xOffset, {
                onChange: animation.renderAll.bind(animation),
                duration: data.deceleration_coefficient_ms*0.75,
            });
        }
        if(objectName === 'group') {
            animationObjectsBall[objectName].animate('angle', angle, {
                onChange: animation.renderAll.bind(animation),
                duration: data.deceleration_coefficient_ms*0.75,
            });
        }
        });
    animation.renderAll();
}


function enableSubmit() {
    submitButton.attr('disabled', false);
}

function disableSubmit() {
    submitButton.attr('disabled', true);
}

function initAnimationBall() {
    animationCtx = $('#animation');

    const width = 950, height = 350;

    animation = new fabric.StaticCanvas('animation');
    animation.setWidth(width);
    animation.setHeight(height);

    animationObjectsBall.ball = new fabric.Circle({
        radius: 15,
        top: 95,
        left: 550,
        stroke: '#e21313',
        strokeWidth: 2,
        fill: '#e21313',
    });
    animationObjectsBall.ground = new fabric.Rect({
        top: 122.5,
        left: 247.5,
        width: 900,
        height: 10,
        stroke: '#443f3f',
        strokeWidth: 2,
        fill: '#b1b1b1',
    });
    let fixStick = new fabric.Rect({
        top: 130,
        left: 50,
        width: 200,
        height: 100,
        stroke: '#443f3f',
        strokeWidth: 2,
        fill: '#b1b1b1',
    })

    animation.add(fixStick);
    animation.add(animationObjectsBall.ground);
    animation.add(animationObjectsBall.ball);

    animationObjectsBall.group = new fabric.Group([animationObjectsBall.ground,animationObjectsBall.ball], {
        originX: 'left',
        originY: 'center',
        objectCaching: false
    });
    animation.add(animationObjectsBall.group);

    animation.renderAll();
}


function initBallChart() {
    // noinspection JSValidateTypes
    chartJS = new Chart(document.getElementById("chart").getContext("2d"), getDefaultBallChartConfig());
}
function getDefaultBallChartConfig() {
    let xMaxRaw = Math.max(...data.angle, ...data.x);
    let xMinRaw = Math.min(...data.angle, ...data.x);
    return {
        data: {
            datasets: [
                {
                    label: translates['common.position'],
                    data: [],
                    borderWidth: 3,
                    pointHitRadius: 0,
                    pointHoverRadius: 0,
                    pointRadius: 0,
                    borderColor: 'rgba(89,220,75,0.89)',
                    backgroundColor: 'rgba(175,246,124,0.1)'
                },
                {
                    label: translates['common.angleDeg'],
                    data: [],
                    borderWidth: 3,
                    pointHitRadius: 0,
                    pointHoverRadius: 0,
                    pointRadius: 0,
                    borderColor: 'rgba(5,17,68,0.89)',
                    backgroundColor: 'rgba(71,108,246,0.1)'
                }
            ],
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        max: 5,
                        stepSize: 1
                    },
                    type: "linear"
                }],
                yAxes: [{
                    ticks: {
                        max: xMaxRaw+10,
                        min: xMinRaw-10,
                        stepSize: 1
                    },
                    type: "linear"
                }]
            },
            tooltips: {
                enabled: false
            }
        },
        type: 'line'
    };
}

function stopDataUpdaterBall() {
    if(intervalUpdater !== null) {
        clearInterval(intervalUpdater);
        intervalUpdater = null;
    }
}
