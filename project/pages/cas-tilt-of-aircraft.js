let responseData = null;
let responseIndex = 0;
let canvas = null;
let chart = null;
let updater = null;
let chartVisible = true;
let animationVisible = true;
let config = null;

function setChartConfig(labels) {
    config = {
        type: 'line',
        data: {
            labels: [],
            datasets: [
                {
                    label: labels['common.flapAngle'],
                    data: [],
                    pointRadius: 0,
                    pointHitRadius: 0,
                    pointHoverRadius: 0,
                    borderColor: '#22a2ff',
                    backgroundColor: 'rgba(34,162,255,0.1)'
                },
                {
                    label: labels['common.aircraftTilt'],
                    data: [],
                    pointRadius: 0,
                    pointHitRadius: 0,
                    pointHoverRadius: 0,
                    borderColor: '#ff1a1a',
                    backgroundColor: 'rgba(255,26,26,0.1)'
                }
            ],
        },
        options: {
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: labels['common.time']
                    },
                    ticks: {
                        beginAtZero: true
                    },
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: labels['common.angle']
                    }
                }]
            },
            title: {
                fontStyle: 'italic',
                fontSize: 16,
                fontColor: 'rgb(133, 133, 133)',
            },
        }
    };
}
function show(targetForm) {
    getData('getTiltOfAircraft', getDataFromForm(targetForm), (response) =>
    {
        console.log(response);
        if(updater != null)
            return;
        responseData = response;

        if(chartVisible)
        {
            showChart();
            $('#cas-result-chart').css('display', 'block');
        }
        else
            $('#cas-result-chart').css('display', 'none');

        if(animationVisible) {
            showAnimation();
            $('#cas-result-animation').css('display', 'block');
        }
        else
            $('#cas-result-animation').css('display', 'none');

        $("#startPosition")[0].value = "["+responseData.result.angles[0]+","+responseData.result.angles[1]+","+responseData.result.angles[2]+"]";

        updater = setInterval(updateTimer, responseData.result.deceleration_coefficient_ms);
    });
};

function updateTimer()
{
    if(responseData.result.flap[responseIndex] != undefined && responseData.result.tilt[responseIndex] != undefined
        && responseData.result.time[responseIndex] != undefined)
    {
        if(chartVisible)
            updateChart(responseIndex);
        if(animationVisible)
            renderAnimation(responseIndex);
    }

    responseIndex-=-1;
    if(responseIndex > responseData.result.time.length)
    {
        clearInterval(updater);
        updater = null;
        responseIndex ^= responseIndex;
    }
}

function updateChart(index)
{
    chart.data.datasets[0].data.push(rad2deg(responseData.result.flap[index]));
    chart.data.datasets[1].data.push(rad2deg(responseData.result.tilt[index]));
    chart.data.labels.push(responseData.result.time[index]);
    chart.update();
}

function showChart()
{
    if(chart == null)
        chart = new Chart(document.getElementById('chart').getContext('2d'), config);

    chart.data.datasets[0].data = [];
    chart.data.datasets[1].data = [];
    chart.data.labels = [];
    chart.update();
}

function renderAnimation(index)
{
    canvas.getObjects()[0].angle = -rad2deg(responseData.result.tilt[index]);
    canvas.getObjects()[0].getObjects()[1].angle  = rad2deg(responseData.result.flap[index]);
    canvas.getObjects()[1].width = canvas.width*(parseFloat(index+1)/parseFloat(responseData.result.time.length))
    canvas.renderAll();
}

function showAnimation()
{
    canvas = new fabric.StaticCanvas('fabric-canvas');
    let plane = new fabric.Path('M 418.08179,143.65638 H 121.5173 L 82.763583,112.72922 C 80.637388,111.03177 77.843646,110.089 74.943871,110.089 h -28.91938 c -1.394105,0 -2.69693,0.59915 -3.471432,1.59746 -0.774503,0.99753 -0.919261,2.26328 -0.385408,3.3719 6.665868,12.8725 18.414241,38.91928 18.414241,38.91928 l 18.703804,38.15498 c 7.472108,15.51516 25.065304,25.63006 44.578164,25.63006 h 26.31281 l -55.416593,72.8588 c -3.491717,4.59082 0.334696,10.68388 6.710513,10.68388 h 21.58558 c 10.62083,0 21.07569,-2.26645 30.42966,-6.59619 l 19.91025,-9.21656 h 67.77913 c 7.63808,0 13.83041,-5.32964 13.83041,-11.90361 0,-6.57396 -6.19233,-11.9036 -13.83041,-11.9036 h -16.34846 l 27.40818,-12.68766 h 56.11549 c 7.63807,0 13.83041,-5.32964 13.83041,-11.90361 0,-6.57396 -6.19234,-11.9036 -13.83041,-11.9036 h -4.68482 l 16.04696,-7.42785 h 98.37015 c 23.77631,0 43.05129,-16.58966 43.05129,-37.05355 -9.2e-4,-20.46389 -19.2759,-37.05275 -43.05221,-37.05275 z');
    let flap = new fabric.Rect({
        left: 80,
        top: 160,
        rx: 20,
        ry: 20,
        width: 100,
        height: 10,
        originX: 'right',
        originY: 'center',
        fill: 'black',
        objectCaching: false
    });

    let group = new fabric.Group([plane,flap], {
        originX: 'center',
        originY: 'center',
        objectCaching: false,
        left: 250,
        top: 250
    });

    if(responseData.result.tilt.length > 0)
    {
        group.angle = -rad2deg(responseData.result.tilt[0]);
    }

    if(responseData.result.flap.length > 0)
    {
        flap.angle = rad2deg(responseData.result.flap[0]);
    }

    let progressBar = new fabric.Rect({
        originX: 'left',
        originY: 'bottom',
        left: 0,
        top: 500,
        width: 0,
        height: 10,
        fill: 'black',
        objectCaching: false
    });


    canvas.add(group);
    canvas.add(progressBar);
}
