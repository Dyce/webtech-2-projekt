<?php

require('../config.php');

$FETranslates = json_encode(
    array (
        'common.angleDeg' => Translator::Inst()->getTranslate('common.angleDeg'),
        'common.position' => Translator::Inst()->getTranslate('common.position')
    )
);

echo "
    <!DOCTYPE html>
    <html lang='sk'>
        <head>
            ".Page::getBaseHeaderImportsAndSetup()."
            <script src='../node_modules/chart.js/dist/Chart.bundle.min.js'></script>
            <script src='../node_modules/fabric/dist/fabric.min.js'></script>
            <script src='cas-inverted-pendulum.js'></script>
            <title>CAS - ".Translator::Inst()->getTranslate('common.invertedPendulum')."</title>
        </head>
        <body onload='init($FETranslates)'>
            ".Page::getNavigationHeader()."
            <div class='container'>
                <h4 class='text-center'>
                    CAS - ".Translator::Inst()->getTranslate('common.invertedPendulum'). "
                </h4>
                <div class='block description-block'>
                    " .Translator::Inst()->getTranslate('description.casInvertedPendulum'). "
                    <div class='description-source'>
                        " .Translator::Inst()->getTranslate('common.source'). ": 
                        <a href='https://en.wikipedia.org/wiki/Inverted_pendulum' target='_blank'>Wikipedia</a>
                    </div>
                </div>
                <form class='block' enctype='multipart/form-data' onsubmit='show(event.target); return false;'>
                    <div class='form-group row mb-0 mb-md-2'>
                        <label class='col-6 col-md-3 col-form-label'>
                            " .Translator::Inst()->getTranslate('common.showGraph')."
                        </label>
                        <div class='col-6 col-md-3 d-flex align-items-center'>
                            <input type='checkbox' class='ml-auto ml-md-0' checked onchange='visible.graph = !visible.graph;'>
                        </div>
                        <label class='col-6 col-md-3 col-form-label d-flex'>
                            <span class='ml-md-auto'>
                                ".Translator::Inst()->getTranslate('common.showAnimation')."
                            </span>
                        </label>
                        <div class='col-6 col-md-3 d-flex align-items-center'>
                            <input type='checkbox' class='ml-auto' checked onchange='visible.animation = !visible.animation;'>
                        </div>
                    </div>
                    <div class='form-group row'>
                        <label class='col-5 col-md-3 col-form-label'>
                            ".Translator::Inst()->getTranslate('common.toPosition')."
                        </label>
                        <div class='col-7 col-md-9'>
                            <input type='number' class='form-control' name='position' required step='1' min='-350' max='350'>
                        </div>
                    </div>
                    <div class='d-flex'>
                        <button type='submit' class='btn btn-success mx-auto'>
                            ".Translator::Inst()->getTranslate('common.show'). "
                        </button>
                    </div>
                </form>
                <div id='cas-result-chart' class='position-relative block'>
                    <canvas id='chart'></canvas>
                </div>
                <div id='cas-result-animation' class='position-relative block'>
                    <canvas id='animation'></canvas>
                </div>
            </div>
        </body>
    </html>
";
