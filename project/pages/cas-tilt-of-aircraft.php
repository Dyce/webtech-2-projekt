<?php

require('../config.php');

$labels = json_encode(
    array (
        'common.flapAngle' => Translator::Inst()->getTranslate('common.flapAngle'),
        'common.aircraftTilt' => Translator::Inst()->getTranslate('common.aircraftTilt'),
        'common.time' => Translator::Inst()->getTranslate('common.time'),
        'common.angle' => Translator::Inst()->getTranslate('common.angle')
    )
);

echo "
    <!DOCTYPE html>
    <html lang='sk'>
        <head>
            ".Page::getBaseHeaderImportsAndSetup()."
            <title>CAS - ".Translator::Inst()->getTranslate('common.tiltOfAircraft')."</title>
            <script src='../node_modules/chart.js/dist/Chart.bundle.min.js'></script>
            <script src='../node_modules/fabric/dist/fabric.min.js'></script>
            <script src='cas-tilt-of-aircraft.js'></script>
        </head>
        <body onload='setChartConfig($labels)'>
            ".Page::getNavigationHeader()."
            <div class='container'>
                <h4 class='text-center'>
                    CAS - ".Translator::Inst()->getTranslate('common.tiltOfAircraft'). "
                </h4>
                <div class='block description-block'>
                    " .Translator::Inst()->getTranslate('description.tiltOfAircraft'). "
                    <div class='description-source'>
                        " .Translator::Inst()->getTranslate('common.source'). ": 
                        <a href='http://ctms.engin.umich.edu/CTMS/index.php?example=AircraftPitch&section=SystemModeling' target='_blank'>umich.edu</a>
                    </div>
                </div>
                <form class='block' enctype='multipart/form-data' onsubmit='show(event.target); return false;'>
                    <div class='form-group row mb-0 mb-md-2'>
                        <label class='col-6 col-md-3 col-form-label'>
                            " .Translator::Inst()->getTranslate('common.showGraph')."
                        </label>
                        <div class='col-6 col-md-3 d-flex align-items-center'>
                            <input type='checkbox' class='ml-auto ml-md-0' checked onchange='chartVisible = this.checked'>
                        </div>
                        <label class='col-6 col-md-3 col-form-label d-flex'>
                            <span class='ml-md-auto'>
                                ".Translator::Inst()->getTranslate('common.showAnimation')."
                            </span>
                        </label>
                        <div class='col-6 col-md-3 d-flex align-items-center'>
                            <input type='checkbox' class='ml-auto' checked onchange='animationVisible = this.checked'>
                        </div>
                    </div>
                    <div class='form-group row'>
                        <label class='col-5 col-md-3 col-form-label'>
                            ".Translator::Inst()->getTranslate('common.angleDeg')."
                        </label>
                        <div class='col-7 col-md-9'>
                            <input type='number' class='form-control' name='angle' min='-90' max='90' required>
                            <input id='startPosition' type='hidden' name='startPosition' value='[0,0,0]'>
                        </div>
                    </div>
                    <div class='d-flex'>
                        <button type='submit' class='btn btn-success mx-auto'>
                            ".Translator::Inst()->getTranslate('common.show'). "
                        </button>
                    </div>
                </form>
                <div id='cas-result-chart' class='position-relative block'>
                    <canvas id='chart'></canvas>                 
               </div>
               <div id='cas-result-animation' class='position-relative block' style='height: 550px'>
                    <canvas id='fabric-canvas' width='500px' height='500px' class='mx-auto d-flex'></canvas>
                </div>
            </div>
        </body>
    </html>
";
