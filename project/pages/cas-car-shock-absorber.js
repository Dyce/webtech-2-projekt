let chart = null;
let chartConfig = null;
let submitButton = null;
let translatedLabels;
let data = {deceleration_coefficient_ms: 0, poziciaAuta: [],  poziciaKolesa: [], time: []};
let chartVisible = true;
let position = {actual: 0, previous: 0};
let intervalIndex = 0;
let maxIntervalIndex = 0;
let intervalUpdater = null;

function enableForm() {
    submitButton.attr('disabled', false);
    position.previous = position.actual;
}

function setChartConfig(labels) {
    submitButton = $(document.getElementsByTagName('form')).find(':submit');
    translatedLabels = labels;

    let xMaxRaw = Math.max(...data.poziciaKolesa, ...data.poziciaAuta);
    let xMinRaw = Math.min(...data.poziciaKolesa, ...data.poziciaAuta);
    let stepSizeRaw = Math.abs(Math.abs(xMaxRaw) - Math.abs(xMinRaw)) / 5;

    let stepSize, xMax, xMin;
    if(Math.log10(stepSizeRaw) >= 0.25) {
        stepSize = Math.ceil(stepSizeRaw);
    } else {
        stepSize = Math.ceil(stepSizeRaw * 100) / 100;
    }

    xMax = Math.ceil(xMaxRaw/stepSize) * stepSize;
    xMin = Math.ceil(Math.abs(xMinRaw)/stepSize) * stepSize * (Math.abs(xMinRaw) / xMinRaw);

    /*chartConfig = {
        data: {
            labels: [],
            datasets: [
                {
                    data: [],
                    backgroundColor: '#347fdb',
                    borderColor: '#347fdb',
                    pointBackgroundColor: '#347fdb',
                    pointBorderColor: '#347fdb',
                    label: translatedLabels["common.positionOfCar"]
                },
                {
                    data: [],
                    backgroundColor: '#cc972e',
                    borderColor: '#cc972e',
                    pointBackgroundColor: '#cc972e',
                    pointBorderColor: '#cc972e',
                    label: translatedLabels["common.positionOfWheel"]
                },

            ]
        },
        options: {
            responsive: false,
            layout: {padding: {top: 12, left: 12, bottom: 12}},
            scales: {
                xAxes: [{
                    gridLines: {color: 'rgba(0,0,0,0.09)', lineWidth: 3, borderDash: [1, 7]}
                }],
                yAxes: [{
                    gridLines: {color: 'rgba(0,0,0,0.09)', lineWidth: 3, borderDash: [1, 7]}
                }]
            },
            plugins: {
                datalabels: {
                    display: true,
                    color: 'rgba(0,0,0,0)',
                    font: {
                        style: ' bold',
                    },
                },
            },
            legend: {
                position: 'bottom',
                labels: {
                    fontStyle: ' bold',
                    fontFamily: 'Open Sans',
                    usePointStyle: true,

                    generateLabels: function (chart) {
                        return chart.data.datasets.map(function (dataset, i) {
                            return {
                                text: dataset.label,
                                lineCap: dataset.borderCapStyle,
                                lineDash: [],
                                lineDashOffset: 0,
                                lineJoin: dataset.borderJoinStyle,
                                pointStyle: 'circle',
                                fillStyle: dataset.backgroundColor,
                                strokeStyle: dataset.borderColor,
                                lineWidth: dataset.pointBorderWidth,
                                lineDash: dataset.borderDash,
                            }
                        })
                    },

                },
            },
            title: {
                display: true,
                text: translatedLabels['common.carShockAbsorber'],
                fontColor: '#3498db',
                fontSize: 32,
                fontStyle: ' bold',
            },
            elements: {
                arc: {},
                point: {pointStyle: 'line',},
                line: {
                    tension: 0.4, fill: false,
                },
                rectangle: {},
            },
            tooltips: {
                mode: 'point',
            },
            hover: {
                mode: 'point',
                animationDuration: 400,
            }
        }
    };*/

    chartConfig = {data: {
        datasets: [
            {
                label: translatedLabels['common.positionOfCar'],
                data: [],
                borderWidth: 3,
                pointHitRadius: 0,
                pointHoverRadius: 0,
                pointRadius: 0,
                borderColor: 'rgba(255,213,0,0.89)',
                backgroundColor: 'rgba(255,213,0,0.1)'
            },
            {
                label: translatedLabels['common.positionOfWheel'],
                data: [],
                borderWidth: 3,
                pointHitRadius: 0,
                pointHoverRadius: 0,
                pointRadius: 0,
                borderColor: 'rgba(0,55,255,0.89)',
                backgroundColor: 'rgba(0,55,255,0.1)'
            }
        ],
    },
    options: {
        legend: {
            labels: {
                fontSize: 20
            }
        },
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero: true,
                    max: data.time[maxIntervalIndex],
                    min: data.time[0],
                    stepSize: 1,
                    fontSize: 12,
                },
                type: "linear"
            }],
                yAxes: [{
                ticks: {
                    max: xMax,
                    min: xMin,
                    stepSize: stepSize,
                    fontSize: 12,
                },
                type: "linear"
            }]
        },
        tooltips: {
            enabled: false
        }
    },
    type: 'line'
};
}

function show(targetForm) {
    let targetFormData = getDataFromForm(targetForm);
    if(+targetFormData.position === +position.previous) {
        return;
    }
    getData('getCarShockAbsorber', {...targetFormData, previousPosition: position.previous}, (response) =>
    {
        console.log(response);

        data = response.result;
        position.actual = +targetFormData.position;

        maxIntervalIndex = data.time.length - 1;
        intervalIndex = 0;

        if(chartVisible) {
            $('#cas-result-chart').css('display', 'block');
            if(chart !== null) {
                chart.destroy();
            }
            showChart();
        } else {
            $('#cas-result-chart').css('display', 'none');
        }

        if(chartVisible) {
            startDataFlow();
        } else {
            enableForm();
        }
    }, () => enableForm());
}

function showChart()
{
        chart = new Chart(document.getElementById('chart').getContext('2d'), chartConfig);
}

function startDataFlow() {
    intervalUpdater = setInterval(() => {
        if(intervalIndex >= maxIntervalIndex) {
            stopDataFlow();
            enableForm();
        }

        if(chartVisible) {
            chart.data.datasets[0].data.push({x: data.time[intervalIndex], y: data.poziciaAuta[intervalIndex]});
            chart.data.datasets[1].data.push({x: data.time[intervalIndex], y: data.poziciaKolesa[intervalIndex]});
            chart.update();
        }

        intervalIndex++;
    }, data.deceleration_coefficient_ms);
}

function stopDataFlow() {
    if(intervalUpdater !== null) {
        clearInterval(intervalUpdater);
        intervalUpdater = null;
    }
}