// noinspection JSUnusedGlobalSymbols
function downloadLogExport(type) {
    getData('getLogExport', {type: type}, (response) => {
        const url = window.location.origin + response.result;
        const fileName = getFileNameWithDate(" log_export." + type);
        const tmpLink = $('<a href="'+ url +'" download="'+ fileName +'"></a>');

        tmpLink[0].click(); tmpLink[0].remove();
    });
}
// noinspection JSUnusedGlobalSymbols
function sendByMail() {
    let statistics = document.querySelector('#statistics').outerHTML;
    var email = document.getElementsByName("email")[0].value;
    getData('getEmailData', {statistics: statistics, email:email },(response) => {
        let data = response.result();
    });
}
