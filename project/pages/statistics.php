<?php

require('../config.php');

$CSVType = json_encode("csv");
$PDFType = json_encode("pdf");

$statistics = "";

echo "
    <!DOCTYPE html>
    <html lang='sk'>
        <head>
            ".Page::getBaseHeaderImportsAndSetup()."
            <script src='statistics.js'></script>
            <title>".Translator::Inst()->getTranslate('common.statistics')."</title>
            <script src='statistics.js'></script>
        </head>
        <body>
            ".Page::getNavigationHeader()."
            <div class='container'>
                <h4 class='text-center'>
                   ".Translator::Inst()->getTranslate('common.statistics'). "
                </h4>
                <div id='statistics' class='block'>
                    <p class='text-center'>
                        ".Translator::Inst()->getTranslate('description.statistics').
                    "</p>
                    <table class='table table-dark table-striped table-bordered'>
                        <thead>
                            <tr>
                                <th>".Translator::Inst()->getTranslate('common.action')."</th>
                                <th>".Translator::Inst()->getTranslate('common.count')."</th>   
                            </tr>
                        </thead>
                        <tbody>";
                            $result = App::Inst()->getStatistics();
                            foreach ($result as $row) {
                                switch ($row['action']) {
                                    case "aircraft":
                                        $row['action'] = Translator::Inst()->getTranslate('common.aircraftTilt');
                                        break;
                                    case "car":
                                        $row['action'] = Translator::Inst()->getTranslate('common.carShockAbsorber');
                                        break;
                                    case "custom":
                                        $row['action'] = Translator::Inst()->getTranslate('common.custom');
                                        break;
                                    case "pendulum":
                                        $row['action'] = Translator::Inst()->getTranslate('common.invertedPendulum');
                                        break;
                                    case "ball":
                                        $row['action'] = Translator::Inst()->getTranslate('common.ballOnStick');
                                        break;
                                    case "getApiExport":
                                        $row['action'] = "API export";
                                        break;
                                    case "getLogExport":
                                        $row['action'] = "Log export";
                                        break;
                                    default:
                                        break;
                                }
                                echo "<tr>";
                                echo "<td>".$row['action']."</td>
                                      <td>".$row['count']."</td>
                                      </tr>";
                            }
echo "
                        </tbody>
                    </table>
                </div>
                <div class='block mb-1'>
                    <p class='text-center'>
                        ".Translator::Inst()->getTranslate('description.sendStatisticsByMail')."
                    </p>
                    <form class='form-inline' method='post' onsubmit='sendByMail()'>
                        <div class='d-flex mx-auto'>
                            <div class='form-group'>
                                <input type='email' class='form-control' name='email' placeholder='Email'/>
                                <button class='btn btn-success ml-2' type='submit'>
                                    ".Translator::Inst()->getTranslate('common.sendMail')."
                                </button>
                            </div>
                        </div>
                    </form>
                    ".$statistics."
                </div>
                <h4 class='text-center'>
                    Log
                </h4>
                <div class='block mt-3'>
                    <p class='text-center'>"
                        .Translator::Inst()->getTranslate('description.log')."
                    </p>
                    <table class='w-100 table table-dark table-striped table-bordered'>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>".Translator::Inst()->getTranslate('common.action')."</th>
                                <th>".Translator::Inst()->getTranslate('common.parameters')."</th>
                                <th>".Translator::Inst()->getTranslate('common.time')."</th>
                                <th>".Translator::Inst()->getTranslate('common.error')."</th>   
                            </tr>
                        </thead>
                        <tbody>";
                            $result = App::Inst()->getLogs();
                            foreach ($result as $row) {
                                $json = json_decode($row['log'], true);
                                //var_dump(get_object_vars($json));
                                unset($json['auth_key']);
                                echo "<tr>";
                                    echo "<td>".$row['id']."</td>
                                          <td>".$json['action']."</td>
                                          <td>";
                                    foreach ($json as $key => $value)
                                    {
                                        if($key == "action")
                                            continue;
                                        echo $key.": ".$value."<br>";
                                    }
                                    echo "</td>
                                          <td>".date('j.n.Y H:i:s', strtotime($row['time']))."</td>
                                          <td>".$row['errorLog']."</td>
                                          </tr>";
                            }
echo "
                        </tbody>
                    </table>
                </div>
                <div class='block'>
                    <h4 class='text-center sub-title'>
                        ".Translator::Inst()->getTranslate('common.logExports')."
                    </h4>
                    <div class='row col-12 col-md-6 mx-auto mt-0'>
                        <button type='button' class='btn btn-success mx-auto' onclick='downloadLogExport($PDFType)'>
                            PDF Export
                        </button>
                        <button type='button' class='btn btn-success mx-auto' onclick='downloadLogExport($CSVType)'>
                            CSV Export
                        </button> 
                    </div>
                </div>
            </div>
        </body>
    </html>
";
