let animation = null;
let animationCtx = null;
let animationObjects = {
    axis: null,
    cart: null,
    invertedPendulum: null,
    pendulumHalfCircle: null,
    textAngle: null,
    wheelLeft: null,
    wheelRight: null
};
let chartJS = null;
let data = {angle: [], deceleration_coefficient_ms: 0, time: [], x: []};
let intervalUpdater = null;
let intervalIndex = 0;
let isSmallScreen = false;
let maxIntervalIndex = 0;
let position = {actual: 0, previous: 0};
let submitButton = null;
let translates = {};
let visible = {animation: true, graph: true};

function disableForm() {
    submitButton.attr('disabled', true);
}

function enableForm() {
    submitButton.attr('disabled', false);
    position.previous = position.actual;
}

// noinspection JSUnusedGlobalSymbols
function init(translatesObj) {
    submitButton = $(document.getElementsByTagName('form')).find(':submit');
    translates = translatesObj;

    isSmallScreen = getIsSmallScreen();
    window.addEventListener("resize", () => {
        if(getIsSmallScreen() !== isSmallScreen) {
            isSmallScreen = !isSmallScreen;

            if(chartJS) {
                let newFontSize = getChartJSFontSize();

                chartJS.options.scales.xAxes[0].ticks.fontSize = newFontSize;
                chartJS.options.scales.xAxes[0].ticks.stepSize = isSmallScreen ? 1 : 0.5;
                chartJS.options.scales.yAxes[0].ticks.fontSize = newFontSize;
                chartJS.options.legend.labels.fontSize = newFontSize;

                chartJS.update();
            }
        }
    });
}

function getDefaultChartConfig() {
    let xMaxRaw = Math.max(...data.angle, ...data.x);
    let xMinRaw = Math.min(...data.angle, ...data.x);
    let stepSizeRaw = Math.abs(Math.abs(xMaxRaw) - Math.abs(xMinRaw)) / 5;

    let stepSize, xMax, xMin;
    if(Math.log10(stepSizeRaw) >= 0.25) {
        stepSize = Math.ceil(stepSizeRaw);
    } else {
        stepSize = Math.ceil(stepSizeRaw * 100) / 100;
    }

    xMax = Math.ceil(xMaxRaw/stepSize) * stepSize;
    xMin = Math.ceil(Math.abs(xMinRaw)/stepSize) * stepSize * (Math.abs(xMinRaw) / xMinRaw);

    return {
        data: {
            datasets: [
                {
                    label: translates['common.position'],
                    data: [],
                    borderWidth: 3,
                    pointHitRadius: 0,
                    pointHoverRadius: 0,
                    pointRadius: 0,
                    borderColor: 'rgba(255,213,0,0.89)',
                    backgroundColor: 'rgba(255,213,0,0.1)'
                },
                {
                    label: translates['common.angleDeg'],
                    data: [],
                    borderWidth: 3,
                    pointHitRadius: 0,
                    pointHoverRadius: 0,
                    pointRadius: 0,
                    borderColor: 'rgba(0,55,255,0.89)',
                    backgroundColor: 'rgba(0,55,255,0.1)'
                }
            ],
        },
        options: {
            legend: {
                labels: {
                    fontSize: getChartJSFontSize()
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        max: data.time[maxIntervalIndex],
                        min: data.time[0],
                        stepSize: isSmallScreen ? 1 : 0.5,
                        fontSize: getChartJSFontSize(),
                    },
                    type: "linear"
                }],
                yAxes: [{
                    ticks: {
                        max: xMax,
                        min: xMin,
                        stepSize: stepSize,
                        fontSize: getChartJSFontSize(),
                    },
                    type: "linear"
                }]
            },
            tooltips: {
                enabled: false
            }
        },
        type: 'line'
    };
}

function initAnimation() {
    animationCtx = $('#animation');

    const width = 950, height = 340;

    animation = new fabric.StaticCanvas('animation');
    animation.setWidth(width);
    animation.setHeight(height);

    let groundUpper = new fabric.Rect({
        top: height/5*4,
        left: 0,
        width: width - 2,
        height: 15,
        stroke: 'rgba(57,130,27,0.8)',
        strokeWidth: 2,
        fill: 'rgba(63,156,21,0.86)'
    });

    let groundLower = new fabric.Rect({
        top: groundUpper.top + groundUpper.height,
        left: 0,
        width: width,
        height: height - groundUpper.top - groundUpper.height,
        fill: 'rgba(97,76,23,0.65)'
    });

    animationObjects.cart = new fabric.Rect({
        top: groundUpper.top - 50 - 12,
        left: width/2 - (width/12),
        width: width/6,
        height: 50,
        stroke: '#443f3f',
        strokeWidth: 2,
        fill: '#b1b1b1',
    });

    animationObjects.pendulumHalfCircle = new fabric.Circle({
        radius: 14,
        left: animationObjects.cart.left + animationObjects.cart.width/2 - 14,
        top: animationObjects.cart.top - 12,
        startAngle: Math.PI,
        endAngle: 2 * Math.PI,
        stroke: '#443f3f',
        strokeWidth: 2,
        fill: '#7d7c7c',
    });

    let stick = new fabric.Rect({
        width: 5,
        height: 140,
        stroke: '#443f3f',
        strokeWidth: 2,
        fill: '#b1b1b1',
    });

    let circle = new fabric.Circle({
        radius: 16,
        left: stick.width/2 - 16 + 1,
        top: stick.height,
        stroke: '#443f3f',
        strokeWidth: 2,
        fill: '#474646',
    });

     animationObjects.invertedPendulum = new fabric.Group([stick, circle], {
         left: animationObjects.pendulumHalfCircle.left + animationObjects.pendulumHalfCircle.radius + circle.radius + 1,
         top: animationObjects.pendulumHalfCircle.top + animationObjects.pendulumHalfCircle.radius/2,
         angle: 180,
    });

    animationObjects.wheelLeft = new fabric.Circle({
        radius: 16,
        top: animationObjects.cart.top + animationObjects.cart.height - 10,
        left: animationObjects.cart.left + 10,
        stroke: '#443f3f',
        strokeWidth: 2,
        fill: '#7d7c7c',
    });

    animationObjects.wheelRight = new fabric.Circle({
        radius: 16,
        top: animationObjects.wheelLeft.top,
        left: animationObjects.cart.left + animationObjects.cart.width - 10 - 32,
        stroke: '#443f3f',
        strokeWidth: 2,
        fill: '#7d7c7c',
    });

    animationObjects.axis = new fabric.Line(
        [
            animationObjects.invertedPendulum.left - animationObjects.invertedPendulum.width/2,
            animationObjects.invertedPendulum.top - animationObjects.invertedPendulum.height - 25,
            animationObjects.invertedPendulum.left - animationObjects.invertedPendulum.width/2,
            animationObjects.wheelLeft.top + animationObjects.wheelLeft.height
        ], {
        strokeDashArray: [5, 5],
        stroke: '#440d0d',
        strokeWidth: 1.5,
    });

    animationObjects.textAngle = new fabric.Text('0.0 °', {
        left: animationObjects.axis.left + animationObjects.axis.width + 5,
        top: animationObjects.axis.top + 5,
        fontSize: 14,
        fill: '#440d0d'
    });

    animation.add(groundLower);
    animation.add(groundUpper);
    animation.add(animationObjects.invertedPendulum);
    animation.add(animationObjects.pendulumHalfCircle);
    animation.add(animationObjects.cart);
    animation.add(animationObjects.wheelLeft);
    animation.add(animationObjects.wheelRight);
    animation.add(animationObjects.axis);
    animation.add(animationObjects.textAngle);

    animation.renderAll();
}

function initChart() {
    // noinspection JSValidateTypes
    chartJS = new Chart(document.getElementById("chart").getContext("2d"), getDefaultChartConfig());
}

function getIsSmallScreen() {
    return document.body.clientWidth < 768;
}

function getChartJSFontSize() {
    return isSmallScreen ? 9 : 12;
}

// noinspection JSUnusedGlobalSymbols
function show(targetForm) {
    let targetFormData = getDataFromForm(targetForm);
    if(+targetFormData.position === +position.previous) {
        return;
    }

    disableForm();
    getData('getInvertedPendulum', {...targetFormData, previousPosition: position.previous}, (response) => {
        data = response.result;
        position.actual = +targetFormData.position;

        maxIntervalIndex = data.time.length - 1;
        intervalIndex = 0;

        if(visible.graph) {
            $('#cas-result-chart').css('display', 'block');
            if(chartJS !== null) {
                chartJS.destroy();
            }

            initChart();
        } else {
            $('#cas-result-chart').css('display', 'none');
        }

        if(visible.animation) {
            $('#cas-result-animation').css('display', 'block');
            if(animation === null) {
                initAnimation();
            }
        } else {
            $('#cas-result-animation').css('display', 'none');
            moveInversePendulum(data.x[maxIntervalIndex] - position.previous, data.angle[maxIntervalIndex]);
        }

        if(visible.graph || visible.animation) {
            startDataUpdater();
        } else {
            enableForm();
        }

        scrollToEndOfThePage();
    }, () => enableForm());
}

function startDataUpdater() {
    intervalUpdater = setInterval(() => {
        if(intervalIndex >= maxIntervalIndex) {
            stopDataUpdater();
            enableForm();
        }

        if(visible.graph) {
            chartJS.data.datasets[0].data.push({x: data.time[intervalIndex], y: data.x[intervalIndex]});
            chartJS.data.datasets[1].data.push({x: data.time[intervalIndex], y: data.angle[intervalIndex]});
            chartJS.update();
        }

        if(visible.animation) {
            moveInversePendulum(data.x[intervalIndex] - (intervalIndex === 0 ? position.previous : data.x[intervalIndex - 1]), data.angle[intervalIndex]);
        }

        intervalIndex++;
    }, data.deceleration_coefficient_ms);
}

function moveInversePendulum(xOffset, angle) {
    Object.keys(animationObjects).forEach((objectName) => {
        animationObjects[objectName].animate('left', animationObjects[objectName].left + xOffset, {
            onChange: animation.renderAll.bind(animation),
            duration: data.deceleration_coefficient_ms * 0.75,
        });

        if(objectName === 'invertedPendulum') {
            animationObjects[objectName].animate('angle', 180 + angle, {
                onChange: animation.renderAll.bind(animation),
                duration: data.deceleration_coefficient_ms * 0.75,
            });
        } else if(objectName === 'textAngle'){
            animationObjects[objectName].set('text', (Math.round(angle*10)/10).toFixed(1) + " °");
        }
    });

    animation.renderAll();
}

function stopDataUpdater() {
    if(intervalUpdater !== null) {
        clearInterval(intervalUpdater);
        intervalUpdater = null;
    }
}
