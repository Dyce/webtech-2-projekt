// noinspection JSUnusedGlobalSymbols
function downloadAPIExport () {
    let api = document.querySelector('.api').outerHTML;
    getData('getApiExport', {api: api},(response) => {
        const url = window.location.origin + response.result;
        const fileName = getFileNameWithDate(" api_export.pdf");
        const tmpLink = $('<a href="'+ url +'" download="'+ fileName +'"></a>');

        tmpLink[0].click(); tmpLink[0].remove();
    });
}
