const authKey = '83ad3a08cea69b2ac717da98e9059418';

function getData(actionName, inData, onComplete = () => {}, onError = () => {}) {
    $.get('/project/api/' + actionName, {...inData, auth_key: authKey}, (result) => {
        onComplete(JSON.parse(result));
    }).fail(() => onError());
}

function getDataFromForm(targetForm) {
    return $(targetForm).serializeArray().reduce((obj, item) =>{
        obj[item.name] = item.value;
        return obj;
    }, {});
}

function scrollToEndOfThePage() {
    $('html,body').animate({scrollTop: document.body.scrollHeight}, 780);
}

function selectLanguage(languageCode) {
    document.cookie = "language_code=" + languageCode;
    document.location.reload();
}

function rad2deg(radians) {
    return radians*(180/Math.PI);
}

function deg2rad(degrees) {
    return degrees*(Math.PI/180);
}


function getFileNameWithDate(originalFileName) {
    const date = new Date();

    return date.getFullYear().toString() + "-" +
        (date.getMonth() + 1).toString().padStart(2, "0") + "-" +
        date.getDate().toString().padStart(2, "0") + " " +
        date.getHours().toString().padStart(2, "0") + "_" +
        date.getMinutes().toString().padStart(2, "0") + originalFileName;
}
