<?php

require('../config.php');


echo "
    <!DOCTYPE html>
    <html lang='sk'>
        <head>
            ".Page::getBaseHeaderImportsAndSetup()."
            <script src='api.js'></script>
            <title>API</title>
        </head>
        <body>
            ".Page::getNavigationHeader()."
            <div class='container'>
                <div class='api'>
<h4 class='text-center'>
    API ".Translator::Inst()->getTranslate("common.apiDocumentation")."
</h4>
<h5 class='text-md-left'>
    ".Translator::Inst()->getTranslate("common.method").": <strong>getEmailData</strong>
</h5>
<pre>".Translator::Inst()->getTranslate("common.type").": <strong>GET</strong><br>".Translator::Inst()->getTranslate("common.parameters").":<br>".Translator::Inst()->getTranslate("api.parametersEmailData")."<br>".Translator::Inst()->getTranslate("common.returnValues").":".Translator::Inst()->getTranslate("api.returnEmailData")."<br>

<h5 class='text-md-left'>
    ".Translator::Inst()->getTranslate("common.method").": <strong>getApiExport</strong>
</h5>
<pre>".Translator::Inst()->getTranslate("common.type").": <strong>GET</strong><br>".Translator::Inst()->getTranslate("common.parameters").":<br>".Translator::Inst()->getTranslate("api.parametersApiExport")."<br>".Translator::Inst()->getTranslate("common.returnValues").":".Translator::Inst()->getTranslate("api.returnValuesApiExport")."<br>

</pre><h5 class='text-md-left'>
    ".Translator::Inst()->getTranslate("common.method").": <strong>getLogExport</strong>
</h5>
<pre>".Translator::Inst()->getTranslate("common.type").": <strong>GET</strong><br>".Translator::Inst()->getTranslate("common.parameters").":<br>".Translator::Inst()->getTranslate("api.parametersLogExport")."<br>".Translator::Inst()->getTranslate("common.returnValues").":".Translator::Inst()->getTranslate("api.returnValuesLogExport")."<br>

</pre>
<h5 class='text-md-left'>
    ".Translator::Inst()->getTranslate("common.method").": <strong>getBallOnStick</strong>
</h5>
<pre>".Translator::Inst()->getTranslate("common.type").": <strong>GET</strong><br>".Translator::Inst()->getTranslate("common.parameters").":<br>".Translator::Inst()->getTranslate("api.parametersBallOnStick")."<br>".Translator::Inst()->getTranslate("common.returnValues").":".Translator::Inst()->getTranslate("api.returnValuesBallOnStick")."<br>

</pre>
<h5 class='text-md-left'>
    ".Translator::Inst()->getTranslate("common.method").": <strong>getCasCustom</strong>
</h5>
<pre>".Translator::Inst()->getTranslate("common.type").": <strong>GET</strong><br>".Translator::Inst()->getTranslate("common.parameters").":<br>".Translator::Inst()->getTranslate("api.parametersCASCustom")."<br>".Translator::Inst()->getTranslate("common.returnValues").":".Translator::Inst()->getTranslate("api.returnValuesCASCustom")."<br>
</pre>
<h5 class='text-md-left'>
    ".Translator::Inst()->getTranslate("common.method").": <strong>getCarShockAbsorber</strong>
</h5>
<pre>".Translator::Inst()->getTranslate("common.type").": <strong>GET</strong><br>".Translator::Inst()->getTranslate("common.parameters").":<br>".Translator::Inst()->getTranslate("api.parametersCarShockAbsorber")."<br>".Translator::Inst()->getTranslate("common.returnValues").":".Translator::Inst()->getTranslate("api.returnValuesCarShockAbsorber")."<br>

</pre>
<h5 class='text-md-left'>
    ".Translator::Inst()->getTranslate("common.method").": <strong>getInvertedPendulum</strong>
</h5>
<pre>".Translator::Inst()->getTranslate("common.type").": <strong>GET</strong><br>".Translator::Inst()->getTranslate("common.parameters").":<br>".Translator::Inst()->getTranslate("api.parametersInvertedPendulum")."<br>".Translator::Inst()->getTranslate("common.returnValues").":".Translator::Inst()->getTranslate("api.returnValuesInvertedPendulum")."<br>

</pre>
<h5 class='text-md-left'>
    ".Translator::Inst()->getTranslate("common.method").": <strong>getTiltOfAircraft</strong>
</h5>
<pre>".Translator::Inst()->getTranslate("common.type").": <strong>GET</strong><br>".Translator::Inst()->getTranslate("common.parameters").":<br>".Translator::Inst()->getTranslate("api.parametersTiltOfAircraft")."<br>".Translator::Inst()->getTranslate("common.returnValues").":".Translator::Inst()->getTranslate("api.returnValuesTiltOfAircraft")."<br>
</pre>
                </div> 
                <button type='button' class='btn btn-success mx-auto' onclick='downloadAPIExport()'>
                    Export to PDF
                </button>    
            </div>
        </body>
    </html> 
";
