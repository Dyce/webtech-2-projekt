<?php

require('../config.php');

$labels = json_encode(
    array (
        'common.carShockAbsorber' => Translator::Inst()->getTranslate('common.carShockAbsorber'),
        'common.positionOfCar' => Translator::Inst()->getTranslate('common.positionOfCar'),
        'common.positionOfWheel' => Translator::Inst()->getTranslate('common.positionOfWheel'),
    )
);

echo "
    <!DOCTYPE html>
    <html lang='sk'>
        <head>
            ".Page::getBaseHeaderImportsAndSetup()."
            <script src='../node_modules/chart.js/dist/Chart.bundle.min.js'></script>
            <script src='../node_modules/fabric/dist/fabric.min.js'></script>
            <script src='cas-car-shock-absorber.js'></script>
            <title>CAS - ".Translator::Inst()->getTranslate('common.invertedPendulum')."</title>
        </head>
        <body onload='setChartConfig($labels)'>
            ".Page::getNavigationHeader()."
            <div class='container'>
                <h4 class='text-center'>
                    CAS - ".Translator::Inst()->getTranslate('common.carShockAbsorber'). "
                </h4>
                <div class='block description-block'>
                    ".Translator::Inst()->getTranslate('description.casCarShockAbsorber')."
                    <div class='description-source'>
                        " .Translator::Inst()->getTranslate('common.source'). ":
                        <a href='http://ctms.engin.umich.edu/CTMS/index.php?example=Suspension&section=SystemModeling'>umich.edu</a>
                    </div>
                </div>
                <form class='block' enctype='multipart/form-data' onsubmit='show(event.target); return false;'>
                    <div class='form-group row mb-0 mb-md-2'>
                        <label class='col-6 col-md-3 col-form-label'>
                            " .Translator::Inst()->getTranslate('common.showGraph')."
                        </label>
                        <div class='col-6 col-md-3 d-flex align-items-center'>
                            <input type='checkbox' class='ml-auto ml-md-0' checked>
                        </div>
                        <label class='col-6 col-md-3 col-form-label d-flex'>
                            <span class='ml-md-auto'>
                                ".Translator::Inst()->getTranslate('common.showAnimation')."
                            </span>
                        </label>
                        <div class='col-6 col-md-3 d-flex align-items-center'>
                            <input type='checkbox' class='ml-auto' checked>
                        </div>
                    </div>
                    <div class='form-group row'>
                        <label class='col-5 col-md-3 col-form-label'>
                            ".Translator::Inst()->getTranslate('common.height')."
                        </label>
                        <div class='col-7 col-md-9'>
                            <input type='number' class='form-control' name='height' required>
                        </div>
                    </div>
                    <div class='d-flex'>
                        <button type='submit' class='btn btn-success mx-auto'>
                            ".Translator::Inst()->getTranslate('common.show'). "
                        </button>
                    </div>
                </form>
                <div id='cas-result-chart' class='position-relative block'>
                    <canvas id='chart'></canvas>                 
               </div>
                <div id='cas-result-animation' class='position-relative block'>
                    <canvas id='animation'></canvas>
                </div>

<canvas id=\"fCanvas\" width=\"500\" height=\"300\" style=\"border:1px solid #000000\"></canvas>
<br>
<div>
    <label for=\"h\">Zadaj výšku prekážky(1 až 30):</label>
    <input type=\"number\" id=\"h\" name=\"h\" min=\"1\" max=\"30\">
<button onclick=\"showAnimation()\">Click me</button>
</div>

<script>
    canvas = new fabric.StaticCanvas('fCanvas');
    let auto = new fabric.Path('M0.000 527 l0 -514 46 -7 c61 -8 1014 -8 1122 1 57 4 82 10 82 19 0 7 8 19 19 26 11 8 21 32 25 63 22 143 117 248 252 276 195 40 386 -130 372 -332 l-3 -49 77 0 c116 0 226 19 254 44 22 19 25 31 31 132 7 130 -6 213 -45 294 -33 68 -56 80 -234 125 -167 41 -302 61 -565 81 -236 19 -206 9 -483 160 -256 139 -356 169 -607 179 -76 3 -184 8 -240 10 l-103 6 0 -514z m413 439 c84 -16 166 -50 291 -123 67 -40 76 -48 58 -54 -24 -9 -36 -41 -28 -75 8 -29 37 -28 -314 -9 -146 8 -277 15 -291 15 -25 0 -28 5 -58 125 -17 68 -31 130 -31 136 0 16 261 5 373 -15z m1759 -482 c17 -16 48 -72 48 -88 0 -8 -98 -18 -117 -12 -17 6 -83 101 -83 120 0 17 130 -1 152 -20z');
    let koleso1 = new fabric.Path('M7554 15976 c-123 -39 -210 -153 -226 -295 -3 -25 -5 -25 -208 -48 -113 -12 -270 -33 -348 -46 -129 -21 -144 -22 -149 -8 -18 59 -54 118 -95 157 -68 64 -126 87 -218 87 -66 0 -119 -13 -425 -102 -369 -108 -417 -128 -470 -197 -53 -70 -70 -122 -69 -219 l1 -90 -141 -54 c-77 -29 -217 -88 -310 -129 l-169 -75 -21 29 c-33 46 -87 92 -136 116 -54 27 -173 36 -233 18 -21 -6 -179 -95 -351 -196 -340 -202 -398 -248 -430 -340 -20 -60 -20 -176 0 -229 8 -22 17 -47 19 -55 3 -10 -37 -45 -118 -103 -68 -48 -185 -137 -260 -197 l-137 -110 -44 34 c-61 46 -107 61 -196 61 -65 0 -84 -4 -140 -32 -56 -27 -103 -70 -336 -305 -248 -249 -273 -278 -293 -331 -23 -63 -28 -157 -11 -219 6 -20 26 -60 45 -88 19 -29 35 -54 35 -56 0 -2 -42 -54 -93 -116 -50 -62 -148 -188 -216 -280 l-123 -167 -42 22 c-33 18 -57 22 -131 22 -78 0 -97 -4 -147 -28 -32 -15 -74 -48 -95 -71 -38 -44 -354 -582 -395 -672 -16 -36 -22 -69 -22 -124 -1 -66 4 -85 32 -143 23 -46 47 -78 80 -104 26 -20 50 -40 55 -43 4 -4 -13 -51 -39 -106 -38 -83 -203 -489 -216 -532 -2 -9 -31 -13 -98 -15 -76 -2 -104 -7 -138 -25 -64 -34 -116 -86 -146 -145 -15 -30 -64 -201 -117 -412 -97 -387 -98 -395 -61 -499 22 -65 97 -145 161 -176 34 -15 51 -30 51 -41 0 -10 -9 -74 -20 -141 -17 -105 -60 -461 -60 -494 0 -6 -24 -14 -54 -17 -104 -10 -200 -79 -254 -180 l-27 -52 -3 -406 c-3 -390 -2 -408 18 -461 41 -110 138 -188 261 -210 32 -5 59 -15 59 -21 0 -59 62 -525 86 -648 5 -24 0 -28 -55 -48 -75 -27 -143 -91 -179 -169 -47 -101 -41 -152 66 -541 51 -183 99 -349 108 -369 24 -59 79 -117 146 -153 60 -32 68 -34 162 -34 90 0 100 -2 107 -20 5 -11 34 -85 65 -165 31 -80 85 -210 121 -289 35 -79 63 -147 61 -151 -2 -4 -23 -23 -48 -42 -114 -86 -158 -253 -100 -384 38 -87 364 -641 399 -677 53 -56 128 -89 214 -95 70 -4 89 0 188 42 12 5 38 -23 110 -121 51 -70 147 -194 211 -275 65 -81 118 -149 118 -153 0 -3 -13 -23 -29 -43 -44 -57 -66 -127 -65 -207 1 -127 11 -141 325 -449 290 -284 322 -308 421 -326 87 -16 202 15 265 73 16 14 26 17 35 10 7 -5 49 -39 93 -75 79 -64 348 -268 395 -299 23 -15 23 -17 8 -50 -63 -133 -38 -287 61 -382 36 -36 635 -380 701 -404 43 -15 152 -17 202 -3 52 15 127 68 164 117 l31 41 116 -53 c114 -53 488 -205 504 -205 5 0 8 -37 8 -82 0 -69 4 -93 26 -139 32 -68 95 -134 153 -160 23 -10 196 -61 384 -114 377 -105 419 -111 516 -75 88 33 155 105 202 218 1 1 55 -6 120 -17 114 -20 364 -52 505 -66 l67 -7 11 -56 c6 -31 20 -75 32 -99 28 -58 108 -127 176 -153 54 -20 70 -21 462 -18 403 3 406 3 453 27 97 47 173 151 185 251 l6 48 71 7 c128 12 452 55 533 70 43 8 83 15 89 15 6 0 20 -22 32 -48 34 -75 85 -127 160 -164 109 -54 144 -50 542 65 184 53 359 108 388 123 110 56 172 164 172 300 0 62 3 76 18 81 107 37 377 149 523 216 l77 35 30 -43 c79 -113 244 -165 370 -117 41 16 506 286 636 369 89 57 145 151 153 257 5 57 -4 96 -40 186 -4 11 24 36 100 90 58 41 178 132 265 203 88 70 162 127 165 127 4 0 21 -12 40 -26 65 -49 122 -68 208 -69 132 0 145 9 471 337 317 319 319 322 319 458 0 65 -5 85 -32 141 -18 36 -41 74 -52 83 -20 18 -19 19 48 100 56 67 140 177 336 440 l20 27 55 -22 c85 -33 183 -30 263 9 34 17 76 46 94 64 37 38 377 619 405 693 27 69 22 181 -11 249 -24 49 -76 109 -120 139 l-25 16 43 98 c53 120 172 409 196 479 l18 52 81 -5 c111 -7 179 19 258 97 67 68 68 70 183 530 65 259 76 314 72 365 -9 115 -76 214 -182 266 -36 17 -65 35 -65 38 0 3 12 83 26 176 14 94 33 247 43 340 l17 170 59 6 c116 11 214 80 266 184 l24 50 0 420 c0 410 -1 421 -22 467 -28 61 -101 134 -161 160 -26 12 -74 24 -107 27 l-60 6 -22 196 c-12 107 -31 253 -43 324 -11 70 -19 129 -18 130 2 1 24 12 50 24 130 61 206 210 178 349 -44 219 -155 694 -170 728 -25 57 -85 121 -140 149 -61 31 -153 46 -210 35 -25 -4 -46 -8 -47 -7 0 1 -26 67 -56 147 -56 149 -164 404 -198 471 l-19 38 34 21 c48 29 94 84 122 144 31 66 33 190 5 251 -44 96 -376 633 -418 677 -65 66 -128 92 -228 92 -60 0 -92 -5 -126 -21 l-46 -20 -91 126 c-50 69 -142 189 -203 266 -62 77 -116 147 -122 156 -7 12 -1 27 27 63 48 63 70 142 63 222 -8 85 -40 147 -112 220 -177 180 -496 479 -534 502 -36 21 -61 27 -132 30 -100 5 -137 -5 -211 -54 -31 -21 -55 -31 -62 -26 -6 5 -56 45 -111 89 -55 44 -171 132 -257 197 l-158 117 22 42 c34 64 32 192 -4 267 -40 82 -84 118 -288 236 -102 58 -246 142 -320 185 -74 42 -157 83 -183 90 -114 28 -241 -13 -324 -106 l-48 -53 -110 50 c-60 28 -198 85 -305 128 l-194 79 2 88 c3 79 0 94 -26 148 -63 129 -107 155 -422 240 -74 20 -207 57 -294 82 -139 39 -169 44 -235 41 -131 -6 -225 -70 -282 -192 l-29 -63 -108 19 c-135 23 -493 68 -543 68 -44 0 -53 11 -63 78 -14 94 -83 185 -174 229 l-57 28 -405 2 c-310 2 -415 -1 -446 -11z m957 -2300 c2075 -193 3876 -1502 4707 -3421 471 -1087 587 -2331 326 -3494 -322 -1436 -1188 -2692 -2419 -3506 -223 -148 -375 -235 -630 -360 -641 -316 -1262 -489 -2035 -567 -166 -17 -784 -16 -955 1 -516 51 -960 146 -1401 300 -441 153 -941 406 -1324 669 -1146 788 -1954 1933 -2305 3267 -72 273 -130 607 -162 927 -24 242 -24 784 0 1033 37 382 104 732 204 1070 19 66 40 136 45 155 6 18 37 108 70 199 574 1579 1830 2835 3409 3409 91 33 181 64 199 70 19 5 89 26 155 45 408 121 788 185 1305 221 120 8 655 -4 811 -18z');
    let koleso2 = new fabric.Path('M7700 13104 c-168 -12 -403 -39 -535 -60 -911 -149 -1741 -531 -2465 -1134 -157 -131 -463 -436 -601 -600 -680 -804 -1089 -1769 -1196 -2820 -24 -244 -24 -768 0 -1000 132 -1235 656 -2326 1523 -3169 408 -397 814 -683 1319 -931 609 -299 1202 -458 1905 -511 195 -15 662 -6 860 15 1029 113 1948 502 2750 1166 154 127 503 475 631 630 712 857 1115 1863 1190 2970 21 321 3 766 -46 1090 -252 1678 -1313 3120 -2840 3856 -539 260 -1092 415 -1700 479 -143 15 -672 28 -795 19z m610 -574 c1593 -114 3010 -1063 3729 -2500 246 -492 391 -981 458 -1545 25 -213 25 -777 0 -990 -88 -740 -320 -1389 -714 -1993 -647 -993 -1648 -1692 -2793 -1952 -311 -71 -558 -99 -915 -107 -639 -13 -1237 97 -1805 332 -1706 704 -2820 2368 -2820 4209 0 1038 344 2028 985 2836 213 269 467 524 732 734 544 434 1183 737 1863 885 163 36 453 78 610 90 167 12 503 12 670 1z');
    let koleso3 = new fabric.Path('M7865 12333 c-557 -27 -1050 -134 -1400 -303 -172 -83 -335 -203 -372 -273 -48 -93 52 -272 273 -489 143 -139 198 -186 319 -267 418 -282 871 -457 1233 -478 342 -19 830 122 1147 331 502 330 830 655 836 827 3 57 -8 76 -78 139 -311 279 -897 460 -1648 510 -144 9 -179 10 -310 3z');
    let koleso4 = new fabric.Path('M4924 10926 c-104 -56 -195 -125 -299 -230 -176 -177 -336 -412 -489 -716 -162 -321 -258 -568 -323 -825 -181 -724 -188 -1349 -19 -1734 46 -105 74 -125 176 -118 92 5 170 32 321 108 315 160 647 425 854 684 204 253 316 483 369 750 47 245 51 280 50 565 l0 275 -46 225 c-105 503 -221 824 -349 964 -86 94 -145 107 -245 52z');
    let koleso5 = new fabric.Path('M10885 10945 c-126 -63 -258 -358 -391 -872 -81 -313 -101 -678 -58 -1057 18 -157 24 -184 69 -316 79 -231 232 -498 396 -694 167 -200 544 -489 783 -602 117 -55 174 -73 267 -87 66 -9 82 -8 135 9 56 18 61 22 91 79 41 76 88 224 109 340 22 121 30 457 15 627 -34 383 -134 825 -255 1126 -172 429 -366 777 -580 1042 -172 214 -429 420 -523 420 -15 0 -41 -7 -58 -15z');
    let koleso6 = new fabric.Path('M7855 9949 c-491 -30 -988 -277 -1315 -652 -622 -711 -655 -1749 -81 -2502 76 -100 260 -283 363 -361 365 -275 785 -408 1238 -391 216 8 332 29 535 94 601 194 1079 688 1260 1302 53 181 66 273 72 501 4 170 2 239 -11 330 -114 792 -686 1433 -1450 1625 -195 49 -398 67 -611 54z');
    let koleso7 = new fabric.Path('M4545 6244 c-67 -10 -172 -37 -206 -55 -20 -10 -49 -33 -64 -51 -26 -31 -27 -36 -21 -106 12 -152 113 -388 250 -591 121 -179 227 -304 435 -512 314 -313 525 -476 881 -680 368 -210 716 -347 1033 -405 121 -22 162 -26 293 -23 170 4 188 11 229 80 32 54 28 257 -8 407 -63 269 -168 517 -327 770 -183 292 -426 553 -660 709 -364 242 -707 359 -1275 433 -150 20 -489 34 -560 24z');
    let koleso8 = new fabric.Path('M10964 6230 c-387 -33 -705 -115 -1054 -274 -266 -121 -419 -227 -601 -412 -213 -220 -336 -388 -457 -629 -102 -204 -154 -327 -208 -491 -77 -235 -90 -419 -36 -522 29 -57 57 -71 161 -83 359 -43 942 152 1571 524 178 106 228 140 335 232 527 450 913 947 1046 1345 41 123 37 171 -15 216 -40 35 -133 68 -236 84 -90 15 -385 20 -506 10z');

    let kolesoGroup = new fabric.Group([koleso1,koleso2,koleso3,koleso4,koleso5,koleso6,koleso7,koleso8], {
        originX: 'center',
        originY: 'center',
        objectCaching: false,
        left: 92,
        top: 238
    });
    let autoGroup = new fabric.Group([auto], {
        originX: 'center',
        originY: 'center',
        objectCaching: false,
        left: 50,
        top: 200
    });

    auto.setColor('#00ff80');
    kolesoGroup.scale(0.004);
    autoGroup.scale(0.09);
    autoGroup.set('angle', 180);
    autoGroup.toggle('flipX');
    canvas.add(autoGroup);
    canvas.add(kolesoGroup);

    function showAnimation() {
        bumpheight = document.getElementById('h').value ;
        let podlaha1 = new fabric.Line([0, 270,350, 270], { stroke: 'black'});
        let podlaha2 = new fabric.Line([350, 270,360, 270 - bumpheight], { stroke: 'red' });
        let podlaha3 = new fabric.Line([360, 270 - bumpheight,440, 270 - bumpheight], { stroke: 'black' });
        let podlaha4 = new fabric.Line([440, 270 - bumpheight,450, 270], { stroke: 'blue' });
        let podlaha5 = new fabric.Line([450, 270,1200, 270], { stroke: 'black' });

        let cestaGroup = new fabric.Group([podlaha1,podlaha2,podlaha3,podlaha4,podlaha5], {
            originX: 'center',
            originY: 'center',
            objectCaching: false,
        });

        // autoGroup.animate('left', '+=500', { onChange: canvas.renderAll.bind(canvas),duration: 2000});
        kolesoGroup.animate('angle', '+=650', { onChange: canvas.renderAll.bind(canvas), duration: 2000});
        // kolesoGroup.animate('left', '+=500', { onChange: canvas.renderAll.bind(canvas), duration: 2000});
        cestaGroup.animate('left', '-=500', { onChange: canvas.renderAll.bind(canvas),duration: 2000});

        canvas.add(autoGroup);
        canvas.add(kolesoGroup);
        canvas.add(cestaGroup);
    }
</script>
</body>
</html>
";
