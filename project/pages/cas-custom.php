<?php

require('../config.php');

echo "
    <!DOCTYPE html>
    <html lang='sk'>
        <head>
            ".Page::getBaseHeaderImportsAndSetup()."
            <title>CAS - ".Translator::Inst()->getTranslate('common.custom')."</title>
            <script type='text/javascript' src='cas-custom.js'></script>
        </head>
        <body>
            ".Page::getNavigationHeader()."
            <div class='container'>
                <h4 class='text-center'>
                    CAS - ".Translator::Inst()->getTranslate('common.custom'). "
                </h4>
                <div class='block description-block'>
                    " .Translator::Inst()->getTranslate('description.casCustom'). "
                </div>
                <form class='block mt-0' enctype='multipart/form-data' onsubmit='show(event.target); return false;'>
                    <h4 class='text-center sub-title'>
                        " .Translator::Inst()->getTranslate('common.input')."
                    </h4>
                    <div class='form-group'>
                        <textarea class='form-control' name='customInput' required></textarea>
                    </div>
                    <div class='d-flex'>
                        <button type='submit' class='btn btn-success mx-auto'>
                            ".Translator::Inst()->getTranslate('common.show'). "
                        </button>
                    </div>
                </form>
                <div class='position-relative block'>
                    <h4 class='text-center sub-title'>
                        " .Translator::Inst()->getTranslate('common.output')."
                    </h4>
                    <div id='cas-result-custom' class='form-control mb-2'></div>
                </div>        
            </div>
        </body>
    </html>
";
