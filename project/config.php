<?php
session_start();
ini_set('memory_limit', '-1');

require_once('vendor/autoload.php');

define('API_AUTH_KEY', '83ad3a08cea69b2ac717da98e9059418');

define('DB_DNS', 'mysql:dbname=project;host=0.0.0.0');
define('DB_USER', 'xczapalam');
define('DB_PASSWORD', 'pryskyric_betonarky');

define('DB_ONE', 1);
define('DB_ALL', 2);
define('DB_COL', 3);
define('DB_ROW', 4);
define('DB_QUERY', 5);
define('DB_ASSOC', 6);

define('CAS_DECELERATION_COEFFICIENT_MS', 35);

define('HTTP_BAD_REQUEST', 400);
define('HTTP_UNAUTHORIZED', 401);
define('HTTP_NOT_FOUND', 404);

define('PATH_ROOT', dirname(__FILE__));
define('PATH_LIBRARY', PATH_ROOT."/library");
define('PATH_CONTROLLERS', PATH_ROOT."/controllers");

define('PATH_PUBLIC', PATH_ROOT."/public");
define('PUBLIC_DIR', "/project/public/");

ini_set('include_path',ini_get('include_path').":".PATH_ROOT.":".PATH_LIBRARY.":".PATH_CONTROLLERS);

spl_autoload_register(function ($className) {
    @include_once ("$className.php");
});
