# Webtech 2 Project

## NPM setup
 - Download and install LTS from https://nodejs.org/en/ (in custom setup allow all modules)
 - In project console (or in any console) start command `npm install npm@latest -g`
 - For packages install into project type and start command `npm install` in project console

## Composer
 - On BE is installed composed, for new BE package, define it into composer.json and run `composer install` in BE folder where composer.json is located
 
 
## Connection WEB
 - address: http://wt179.fei.stuba.sk:8179/project/pages/about.php
 - name: xczapalam
 - password: pryskyric_betonarky
 - http port: 8179
 - https port: 4579
 - ssh port: 2379
 
## Connection DB
 - address: http://wt179.fei.stuba.sk:8179/phpmyadmin/index.php
 - name: project
 - user: xczapalam
 - password: pryskyric_betonarky

## Octave setup
 - `apt-get install flatpak`
 - `flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo`
 - `flatpak install flathub org.octave.Octave`
 
 - `apt install liboctave-dev`
 - `apt install octave`
 - run `octave`
 - type `pkg install -forge control`
 - test example: `octave  --eval "1+1"`
 
## Octave custom functions setup
 - Add into /usr/share/octave/site/m/startup/octaverc file
    - `addpath("path to .m files")`
    - `pkg install -global -forge control`
